package GradeSystemsGUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class WeightsDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private float[] weights;
	
	private void weightsSumError() {
		JOptionPane.showMessageDialog(null, "配置有誤，總和不為1", "錯誤", JOptionPane.ERROR_MESSAGE);
	}
	
	public WeightsDialog() {
		
		weights = GradeSystems.getWeights();
		
		setTitle("Set Weights");
		setBounds(100, 100, 452, 379);
		setLocationRelativeTo(this);
		getContentPane().setLayout(new BorderLayout());
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		
		/**
		 * OK & Cancel Button create
		 * 
		 * OK => if array correct then set weights and close the dialog
		 * Cancel => close the dialog
		 */
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JSpinner lab1Spinner = new JSpinner();
		lab1Spinner.setFont(new Font("新細明體", Font.PLAIN, 25));
		lab1Spinner.setModel(new SpinnerNumberModel(new Float(weights[0]), new Float(0), new Float(1), new Float(0.1)));
		lab1Spinner.setBounds(200, 37, 142, 39);
		JSpinner lab2Spinner = new JSpinner();
		lab2Spinner.setFont(new Font("新細明體", Font.PLAIN, 25));
		lab2Spinner.setModel(new SpinnerNumberModel(new Float(weights[1]), new Float(0), new Float(1), new Float(0.1)));
		lab2Spinner.setBounds(200, 86, 142, 39);
		JSpinner lab3Spinner = new JSpinner();
		lab3Spinner.setFont(new Font("新細明體", Font.PLAIN, 25));
		lab3Spinner.setModel(new SpinnerNumberModel(new Float(weights[2]), new Float(0), new Float(1), new Float(0.1)));
		lab3Spinner.setBounds(200, 135, 142, 39);
		JSpinner midTermSpinner = new JSpinner();
		midTermSpinner.setFont(new Font("新細明體", Font.PLAIN, 25));
		midTermSpinner.setModel(new SpinnerNumberModel(new Float(weights[3]), new Float(0), new Float(1), new Float(0.1)));
		midTermSpinner.setBounds(200, 185, 142, 39);
		JSpinner finalExamSpinner = new JSpinner();
		finalExamSpinner.setFont(new Font("新細明體", Font.PLAIN, 25));
		finalExamSpinner.setModel(new SpinnerNumberModel(new Float(weights[4]), new Float(0), new Float(1), new Float(0.1)));
		finalExamSpinner.setBounds(200, 234, 142, 39);
		
		contentPanel.add(lab1Spinner);
		contentPanel.add(lab2Spinner);
		contentPanel.add(lab3Spinner);
		contentPanel.add(midTermSpinner);
		contentPanel.add(finalExamSpinner);
		
		JLabel lblLab1 = new JLabel("Lab1 :");
		lblLab1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLab1.setFont(new Font("新細明體", Font.PLAIN, 25));
		lblLab1.setBounds(50, 37, 140, 39);
		contentPanel.add(lblLab1);
		
		JLabel lblLab2 = new JLabel("Lab2 :");
		lblLab2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLab2.setFont(new Font("新細明體", Font.PLAIN, 25));
		lblLab2.setBounds(50, 86, 140, 39);
		contentPanel.add(lblLab2);
		
		JLabel lblLab3 = new JLabel("Lab3 :");
		lblLab3.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLab3.setFont(new Font("新細明體", Font.PLAIN, 25));
		lblLab3.setBounds(50, 135, 140, 39);
		contentPanel.add(lblLab3);
		
		JLabel lblMidTerm = new JLabel("MidTerm :");
		lblMidTerm.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMidTerm.setFont(new Font("新細明體", Font.PLAIN, 25));
		lblMidTerm.setBounds(50, 185, 140, 39);
		contentPanel.add(lblMidTerm);
		
		JLabel lblFinalExam = new JLabel("FinalExam :");
		lblFinalExam.setHorizontalAlignment(SwingConstants.RIGHT);
		lblFinalExam.setFont(new Font("新細明體", Font.PLAIN, 25));
		lblFinalExam.setBounds(50, 234, 140, 39);
		contentPanel.add(lblFinalExam);
		
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						weights[0] = (Float)lab1Spinner.getValue();
						weights[1] = (Float)lab2Spinner.getValue();
						weights[2] = (Float)lab3Spinner.getValue();
						weights[3] = (Float)midTermSpinner.getValue();
						weights[4] = (Float)finalExamSpinner.getValue();
						if(checkWeightCorrect()) {
							GradeSystems.updateWeights(weights);
							dispose();
						}
						else {
							weightsSumError();
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		
	}
	
	public boolean checkWeightCorrect() {
		float sum = 0;
		for(float weight : this.weights) {
			sum+=weight;
		}
		
		if(sum==1f)
			return true;
		else 
			return false;
	}
}

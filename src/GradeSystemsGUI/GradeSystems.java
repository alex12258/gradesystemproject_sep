package GradeSystemsGUI;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * **********************************************************************
 * GradeSystems 儲存 a list of student grades containsID(ID) GradeSystems()
 * constructor showGrade(ID) showRank(ID) updateWeights()
 */
public class GradeSystems {
	private static float[] weights;
	private static LinkedList<Grades> aList;
	private static final String fileURL = "dataset/gradeInput.txt";
	
	static {
		/**
		 * Default weights set
		 */
		GradeSystems.aList = new LinkedList<Grades>();
		weights = new float[5];

		updateWeights(new float[] { (float) 0.1, (float) 0.1, (float) 0.1, (float) 0.3, (float) 0.4 });

		/**
		 * GradeSystems 建構子 開檔 input file "gradeInput.txt"
		 */
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader( new FileInputStream(fileURL),"UTF-8"));

			String line = null;
			Scanner sc = null;
			while ((line = reader.readLine()) != null && !line.equals("")) {
				sc = new Scanner(line);
				Grades aGrade = new Grades(sc.next(), sc.next() // ID & Name
						, sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt(),sc.nextInt() // all the grades
						, weights); // weight array
				GradeSystems.aList.add(aGrade);
				sc.close();
			}
			reader.close();
		} catch (Exception ex) {
			System.out.println(Arrays.toString(ex.getStackTrace()));
		}
	}

	/**
	 *看 ID 是否含在 aGradeSystem 內
	 * 
	 * @param ID - a user ID
	 */
	public static boolean containsID(String ID) {
		/**
		 * 1. loop aGrade in aList if aGrade.ID is ID then return true end if end loop
		 * 2. return false
		 */
		for (Grades aGrade : GradeSystems.aList) {
			if (aGrade.ID.equals(ID)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * -------------------------------------------------------------- 
	 * 顯示學生成績
	 * 
	 * @param ID - a user ID
	 *---------------------------------------------------------------
	 */
	public static String showGrade(String ID) {
		/**
		 * pseudo code: 1. loop aGrade in aList if aGrade.ID is ID then show 這 ID 的grade
		 * end if end loop
		 */
		StringBuffer grade = new StringBuffer();
		for (Grades aGrade : aList) {
			if (aGrade.ID.equals(ID)) {
				grade.append(aGrade.name + "成績：　\n" );
				grade.append("lab1：" + aGrade.lab1 + "\t\t");
				grade.append("mid-term：" + aGrade.midTerm + "\n");
				grade.append("lab2：" + aGrade.lab2 + "\t\t");
				grade.append("final exam：" + aGrade.finalExam + "\n");
				grade.append("lab3：" + aGrade.lab3 + "\t\t");
				grade.append("total grade：" + aGrade.totalGrade );
			}
		}
		return grade.toString();
	}

	/**
	 * -------------------------------------------------------------- 
	 * 顯示學生排名
	 * 
	 * @param ID - a user ID
	 *---------------------------------------------------------------
	 */
	public static String showRank(String ID) {
		/**
		 * pseudo code: 1. 取得這 ID 的 theTotalGrade 2. 令 rank 為 1 3. loop aGrade in aList
		 * if aGrade.totalGrade > theTotalGrade then rank+1(退1名) end loop 4. show 這 ID
		 * 的rank
		 */
		Grades theGrade = null;
		for (Grades aGrade : aList) {
			if (aGrade.ID.equals(ID)) {
				theGrade = aGrade;
			}
		}
		int theTotalGrade = theGrade.totalGrade;
		int rank = 1;
		for (Grades aGrade : aList) {
			if (aGrade.totalGrade > theTotalGrade) {
				rank += 1;
			}
		}
		return theGrade.name + " 排名第 " + rank;
	}
	
	/**
	 * --------------------------------------------------------------
	 *  取得配分
	 * --------------------------------------------------------------
	 */
	public static float[] getWeights() {
		return weights;
	}
	
	/**
	 * --------------------------------------------------------------
	 *  顯示配分
	 * --------------------------------------------------------------
	 */
	public static String showWeights() {
		StringBuffer weights = new StringBuffer();
		weights.append("\tlab1：" + (int) (GradeSystems.weights[0] * 100) + "%");
		weights.append("\tlab2：" + (int) (GradeSystems.weights[1] * 100) + "%");
		weights.append("\tlab3：" + (int) (GradeSystems.weights[2] * 100) + "%");
		weights.append("\tmid-term：" + (int) (GradeSystems.weights[3] * 100) + "%");
		weights.append("\tfinal exam：" + (int) (GradeSystems.weights[4] * 100) + "%");
		return weights.toString();
	}

	/**
	 * -------------------------------------------------------------- 
	 * 更新配分，重新計算總成績
	 * --------------------------------------------------------------
	 */
	public static void updateWeights(float[] weightsInput) {
		/**
		 * pseudo code: 1. setWeights(weights) 2. loop aGrade in aList
		 * aGrade.calculateTotalGrade(weights) end loop
		 */
		int i = 0;
		for (float weight : weightsInput) {
			GradeSystems.weights[i] = weight;
			// System.out.println(weights[i]);
			++i;
		}

		for (Grades aGrade : aList) {
			aGrade.calculateTotalGrade(weights);
		}
	}
	
}

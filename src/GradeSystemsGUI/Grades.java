package GradeSystemsGUI;


/** **********************************************************************
class Grades 儲存 ID, name, lab1, lab2, lab3, midTerm, finalExam, and totalGrade

calculateTotalGrade(weights)
Grades () { } //建構子
********************************************************************** */

public class Grades {
	/** --------------------------------------------------------------
	 * 需要的字串型態變數, 整數型態變數
	 * public String ID, name;
	 * public int lab1, lab2, lab3, midTerm, finalExam, totalGrade;
	 -------------------------------------------------------------- */
	public String ID, name;
	public int lab1, lab2, lab3, midTerm, finalExam, totalGrade;
	
	public Grades() throws Exception { 
		throw new Exception("Not Implemente");
	}
	
	public Grades(String ID, String name, int lab1, int lab2, int lab3, int midTerm, int finalExam,float[] weights) {
		this.ID = ID;
		this.name = name;
		this.lab1 = lab1;
		this.lab2 = lab2;
		this.lab3 = lab3;
		this.midTerm = midTerm;
		this.finalExam = finalExam;
		this.calculateTotalGrade(weights);
	}
	
	public int calculateTotalGrade(float[] weights) {
		/** 
		 * pseudo code:
		 * Parameter float[] weights
		 * this.totalGrade = Math.round((lab1*weights[0] + lab2*weights[1] + lab3*weights[2] + midTerm*weights[3] + finalExam*weights[4]));
		*/ 
		return this.totalGrade = Math.round((lab1*weights[0] + lab2*weights[1] + lab3*weights[2]
				+ midTerm*weights[3] + finalExam*weights[4]));
	}
}

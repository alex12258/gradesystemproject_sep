package GradeSystemsGUI;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.FontUIResource;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GradeSwing extends JFrame {

	private JPanel contentPane;
	private JTextField inputStudentId;
	private JTextPane outputGradeResult;
	/**
	 * Create the frame.
	 */
	private void NoSuchIDError() {
		JOptionPane.showMessageDialog(null, "學號不存在", "錯誤", JOptionPane.ERROR_MESSAGE);
	}
	
	public GradeSwing() {
		
		/**
		 * 建立視窗
		 */
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			UIManager.put("OptionPane.buttonFont", new FontUIResource(new Font("MingLiU", Font.PLAIN, 18)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		setResizable(false); 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 512);
		setLocationRelativeTo(this);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		/**
		 * Title
		 */
		setTitle("GradeSystems");
		JLabel lblTitle = new JLabel("成績查詢系統");
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setFont(new Font("MingLiU", Font.PLAIN, 36));
		lblTitle.setBounds(175, 11, 250, 50);
		contentPane.add(lblTitle);
		
		/**
		 * 學號窗格建立
		 */
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(SystemColor.activeCaption, 3));
		panel.setBounds(100, 75, 400, 70);
		contentPane.add(panel);
		panel.setLayout(null);
		
		inputStudentId = new JTextField();
		inputStudentId.setBounds(187, 15, 201, 43);
		panel.add(inputStudentId);
		inputStudentId.setColumns(10);
		
		JLabel lblStudentId = new JLabel("請輸入學號：");
		lblStudentId.setFont(new Font("MingLiU", Font.PLAIN, 24));
		lblStudentId.setBounds(12, 13, 149, 43);
		panel.add(lblStudentId);
		
		/**
		 * 成績顯示窗格建立
		 */
		JPanel gradeResultPanel = new JPanel();
		gradeResultPanel.setBounds(47, 286, 500, 177);
		contentPane.add(gradeResultPanel);
		gradeResultPanel.setLayout(null);
		
		JLabel lblgradeResult = new JLabel("查詢結果");
		lblgradeResult.setFont(new Font("MingLiU", Font.PLAIN, 24));
		lblgradeResult.setHorizontalAlignment(SwingConstants.CENTER);
		lblgradeResult.setBounds(153, 10, 200, 39);
		gradeResultPanel.add(lblgradeResult);
		
		outputGradeResult = new JTextPane();
		outputGradeResult.setEditable(false);
		outputGradeResult.setBounds(10, 51, 478, 116);
		gradeResultPanel.add(outputGradeResult);

		StyledDocument doc = outputGradeResult.getStyledDocument();
		SimpleAttributeSet textPosition = new SimpleAttributeSet();
		StyleConstants.setAlignment(textPosition, StyleConstants.ALIGN_CENTER);
		doc.setParagraphAttributes(0, doc.getLength(), textPosition, false);
		
		/**
		 * 功能按鍵建立
		 */
		JButton gradeButton = new JButton("顯示成績 Grade");
		gradeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				outputGradeResult.setFont(new Font("MingLiU", Font.PLAIN, 20));
				StyleConstants.setAlignment(textPosition, StyleConstants.ALIGN_LEFT);
				doc.setParagraphAttributes(0, doc.getLength(), textPosition, false);
				if(GradeSystems.containsID(inputStudentId.getText()))
					outputGradeResult.setText(GradeSystems.showGrade(inputStudentId.getText()));
				else {
					cleanOutputGradeResult();
					NoSuchIDError();
				}
			}
		});
		gradeButton.setFont(new Font("MingLiU", Font.PLAIN, 20));
		gradeButton.setBounds(100, 160, 190, 50);
		contentPane.add(gradeButton);
		
		JButton rankButton = new JButton("顯示排行 Rank");
		rankButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				outputGradeResult.setFont(new Font("MingLiU", Font.PLAIN, 45));
				StyleConstants.setAlignment(textPosition, StyleConstants.ALIGN_CENTER);
				doc.setParagraphAttributes(0, doc.getLength(), textPosition, false);
				if(GradeSystems.containsID(inputStudentId.getText())) {
					outputGradeResult.setText(GradeSystems.showRank(inputStudentId.getText()));
				}
				else {
					cleanOutputGradeResult();
					NoSuchIDError();
				}
			}
		});
		rankButton.setFont(new Font("MingLiU", Font.PLAIN, 20));
		rankButton.setBounds(310, 160, 190, 50);
		contentPane.add(rankButton);
		
		JButton weigthsButton = new JButton("更新配分 Weigths");
		weigthsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							cleanOutputGradeResult();
							WeightsDialog weightsDialog = new WeightsDialog();
							weightsDialog.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		weigthsButton.setFont(new Font("MingLiU", Font.PLAIN, 20));
		weigthsButton.setBounds(100, 223, 190, 50);
		contentPane.add(weigthsButton);
		
		JButton exitButton = new JButton("離開系統 Exit");
		exitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		exitButton.setFont(new Font("MingLiU", Font.PLAIN, 20));
		exitButton.setBounds(310, 223, 190, 50);
		contentPane.add(exitButton);
		
	}
	
	void cleanOutputGradeResult() {
		outputGradeResult.setText("");
		outputGradeResult.repaint();
	}
}
